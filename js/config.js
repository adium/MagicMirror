var config = {
    lang: 'en',
    time: {
        timeFormat: 12,
        displaySeconds: true,
        digitFade: false,
    },
    weather: {
        //change weather params here:
        //units: metric or imperial
        params: {
            //q: 'Ithaca,NY,USA',
            id: '5122432',
            units: 'Imperial',
            // http://openweathermap.org/city/5122432
            lang: 'en',
            APPID: '9ca0875aa20eec2685bb3d53a0b92b7d'
        }
    },
    compliments: {
        interval: 30000,
        fadeInterval: 4000,
        morning: [
            'Either you run the day or the day runs you. -Jim Rohn ',
            'Good, better, best. Never let it rest. &#39;Til your good is better and your better is best. - St. Jerome  ',
            'When you reach the end of your rope, tie a knot in it and hang on. - Franklin D. Roosevelt ',
            'Accept the challenges so that you can feel the exhilaration of victory. - George S. Patton ',
            'In order to succeed, we must first believe that we can. - Nikos Kazantzakis ',
            'Life is 10% what happens to you and 90% how you react to it. - Charles R. Swindoll  ',
            'Failure will never overtake me if my determination to succeed is strong enough. - Og Mandino  ',
            'What you do today can improve all your tomorrows. - Ralph Marston  ',
            'A creative man is motivated by the desire to achieve, not by the desire to beat others. - Ayn Rand ',
            'The secret of getting ahead is getting started. - Mark Twain ',
            'You are never too old to set another goal or to dream a new dream. - C. S. Lewis  ',
            'Always do your best. What you plant now, you will harvest later. - Og Mandino ',
            'It does not matter how slowly you go as long as you do not stop. - Confucius  ',
            'Don&#39;t watch the clock; do what it does. Keep going. - Sam Levenson ',
            'Keep your eyes on the stars, and your feet on the ground. - Theodore Roosevelt ',
            'Go for it now. The future is promised to no one. - Wayne Dyer',
            'Our greatest weakness lies in giving up. The most certain way to succeed is always to try just one more time.',
            'The will to win, the desire to succeed, the urge to reach your full potential... these are the keys that will unlock the door to personal excellence. -Confucius',
            'Believe in yourself! Have faith in your abilities! Without a humble but reasonable confidence in your own powers you cannot be successful or happy. -Norman Vincent Peale',
            'With the new day comes new strength and new thoughts. - Eleanor Roosevelt',
            'If you can dream it, you can do it. - Walt Disney',
            'Aim for the moon. If you miss, you may hit a star. - W. Clement Stone',
            'Start where you are. Use what you have. Do what you can. - Arthur Ashe',
            'Problems are not stop signs, they are guidelines. - Robert H. Schuller',
            'Knowing is not enough; we must apply. Willing is not enough; we must do. - Johann Wolfgang von Goethe',
        ],
        afternoon: [
            'Either you run the day or the day runs you. -Jim Rohn ',
            'Good, better, best. Never let it rest. &#39;Til your good is better and your better is best. - St. Jerome  ',
            'When you reach the end of your rope, tie a knot in it and hang on. - Franklin D. Roosevelt ',
            'Accept the challenges so that you can feel the exhilaration of victory. - George S. Patton ',
            'In order to succeed, we must first believe that we can. - Nikos Kazantzakis ',
            'Life is 10% what happens to you and 90% how you react to it. - Charles R. Swindoll  ',
            'Failure will never overtake me if my determination to succeed is strong enough. - Og Mandino  ',
            'What you do today can improve all your tomorrows. - Ralph Marston  ',
            'A creative man is motivated by the desire to achieve, not by the desire to beat others. - Ayn Rand ',
            'The secret of getting ahead is getting started. - Mark Twain ',
            'You are never too old to set another goal or to dream a new dream. - C. S. Lewis  ',
            'Always do your best. What you plant now, you will harvest later. - Og Mandino ',
            'It does not matter how slowly you go as long as you do not stop. - Confucius  ',
            'Don&#39;t watch the clock; do what it does. Keep going. - Sam Levenson ',
            'Keep your eyes on the stars, and your feet on the ground. - Theodore Roosevelt ',
            'Go for it now. The future is promised to no one. - Wayne Dyer',
            'Our greatest weakness lies in giving up. The most certain way to succeed is always to try just one more time.',
            'The will to win, the desire to succeed, the urge to reach your full potential... these are the keys that will unlock the door to personal excellence. -Confucius',
            'Believe in yourself! Have faith in your abilities! Without a humble but reasonable confidence in your own powers you cannot be successful or happy. -Norman Vincent Peale',
            'With the new day comes new strength and new thoughts. - Eleanor Roosevelt',
            'If you can dream it, you can do it. - Walt Disney',
            'Aim for the moon. If you miss, you may hit a star. - W. Clement Stone',
            'Start where you are. Use what you have. Do what you can. - Arthur Ashe',
            'Problems are not stop signs, they are guidelines. - Robert H. Schuller',
            'Knowing is not enough; we must apply. Willing is not enough; we must do. - Johann Wolfgang von Goethe',
        ],
        evening: [
            'Either you run the day or the day runs you. -Jim Rohn ',
            'Good, better, best. Never let it rest. &#39;Til your good is better and your better is best. - St. Jerome  ',
            'When you reach the end of your rope, tie a knot in it and hang on. - Franklin D. Roosevelt ',
            'Accept the challenges so that you can feel the exhilaration of victory. - George S. Patton ',
            'In order to succeed, we must first believe that we can. - Nikos Kazantzakis ',
            'Life is 10% what happens to you and 90% how you react to it. - Charles R. Swindoll  ',
            'Failure will never overtake me if my determination to succeed is strong enough. - Og Mandino  ',
            'What you do today can improve all your tomorrows. - Ralph Marston  ',
            'A creative man is motivated by the desire to achieve, not by the desire to beat others. - Ayn Rand ',
            'The secret of getting ahead is getting started. - Mark Twain ',
            'You are never too old to set another goal or to dream a new dream. - C. S. Lewis  ',
            'Always do your best. What you plant now, you will harvest later. - Og Mandino ',
            'It does not matter how slowly you go as long as you do not stop. - Confucius  ',
            'Don&#39;t watch the clock; do what it does. Keep going. - Sam Levenson ',
            'Keep your eyes on the stars, and your feet on the ground. - Theodore Roosevelt ',
            'Go for it now. The future is promised to no one. - Wayne Dyer',
            'Our greatest weakness lies in giving up. The most certain way to succeed is always to try just one more time.',
            'The will to win, the desire to succeed, the urge to reach your full potential... these are the keys that will unlock the door to personal excellence. -Confucius',
            'Believe in yourself! Have faith in your abilities! Without a humble but reasonable confidence in your own powers you cannot be successful or happy. -Norman Vincent Peale',
            'With the new day comes new strength and new thoughts. - Eleanor Roosevelt',
            'If you can dream it, you can do it. - Walt Disney',
            'Aim for the moon. If you miss, you may hit a star. - W. Clement Stone',
            'Start where you are. Use what you have. Do what you can. - Arthur Ashe',
            'Problems are not stop signs, they are guidelines. - Robert H. Schuller',
            'Knowing is not enough; we must apply. Willing is not enough; we must do. - Johann Wolfgang von Goethe',
        ]
    },
    calendar: {
        maximumEntries: 10, // Total Maximum Entries
		displaySymbol: true,
		defaultSymbol: 'calendar', // Fontawsome Symbol see http://fontawesome.io/cheatsheet/
        urls: [
		{
			symbol: 'calendar-plus-o', 
			url: 'https://p01-calendarws.icloud.com/ca/subscribe/1/GCAzX3SC7SN5HHWFZa6n9mHknPVEKvPp2WtJIcYKLRDT6hDTQfZfsi_6UqBRhyZJ'
		},
		{
			symbol: 'university',
			url: 'http://outlook.office365.com/owa/calendar/a3667b2a58e24dce9c918422f5d44158@cornell.edu/2b247c7ca7a0459bb483e5bd00d690b93416867231479823292/calendar.ics',
		},
		 {
		     symbol: 'briefcase',
			 url: "https://p01-calendarws.icloud.com/ca/subscribe/1/wO_kAxWgmyBAod6e44aX02YghnZYkWDBoYXurm8FIA-rmJnqROOT_OhuYTqYiQrLqvFaT832IFPyxyTMbWzneLT8-KRSqEz0o5SmyJn_plo",
		 },
		 {
			 symbol: 'truck',
			 url: "https://p01-calendarws.icloud.com/ca/subscribe/1/qSEgXtA5d650gi2TGKhHnsAC9BgtVCIolG5QOKt62gA0iLFEW5zI_QWp1VBWaZPaNEYDHQGZL3Jw5uiT25icogqroLe2MbElFfnd5acgGSw",
		 },
		//{
		//	 symbol: 'gift',
		//	 url: "https://p35-calendars.icloud.com/holidays/us_en.ics",
		// },
		]
    },
    news: {
        feed: 'http://www.nytimes.com/services/xml/rss/nyt/HomePage.xml'
    }
}
